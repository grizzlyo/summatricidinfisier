package com.sda.java;

public class Parametri {

    private int nrLinii;
    private int nrCol;
    private int[][] matrix1;
    private int[][] matrix2;

    Parametri() {
    }

    public Parametri(int nrLinii, int nrCol, int[][] matrix1, int[][] matrix2) {
        this.nrLinii = nrLinii;
        this.nrCol = nrCol;
        this.matrix1 = matrix1;
        this.matrix2 = matrix2;
    }

    public int getNrLinii() {
        return nrLinii;
    }

    public void setNrLinii(int nrLinii) {
        this.nrLinii = nrLinii;
    }

    public int getNrCol() {
        return nrCol;
    }

    public void setNrCol(int nrCol) {
        this.nrCol = nrCol;
    }

    public int[][] getMatrix1() {
        return matrix1;
    }

    public void setMatrix1(int[][] matrix1) {
        this.matrix1 = matrix1;
    }

    public int[][] getMatrix2() {
        return matrix2;
    }

    public void setMatrix2(int[][] matrix2) {
        this.matrix2 = matrix2;
    }
}
