package com.sda.java;

import java.io.*;

public class AdunMatrici {

    Parametri citescMatricile(String patch) {
        Parametri parametri = new Parametri();
        File inputFile = new File(patch);

        try {
            FileReader fileReader = new FileReader(inputFile);
            BufferedReader reader = new BufferedReader(fileReader);

            String linie = reader.readLine();
            String[] dimensiuneMatrice = linie.split(" ");
            int[][] matrice1 = new int[Integer.valueOf(dimensiuneMatrice[0])][Integer.valueOf(dimensiuneMatrice[1])];
            int[][] matrice2 = new int[Integer.valueOf(dimensiuneMatrice[0])][Integer.valueOf(dimensiuneMatrice[1])];
            parametri.setNrLinii(Integer.valueOf(dimensiuneMatrice[0]));
            parametri.setNrCol(Integer.valueOf(dimensiuneMatrice[1]));

            linie = reader.readLine();
            int indexLinie = 0;
            int indexLinie2 = 0;
            while (linie != null) {
                String[] matrice = linie.split(" ");
                if (indexLinie <= 2) {
                    for (int i = 0; i < matrice.length; i++) {
                        matrice1[indexLinie][i] = Integer.valueOf(matrice[i]);
                    }
                    indexLinie++;
                    linie = reader.readLine();
                } else {
                    for (int i = 0; i < matrice.length; i++) {
                        matrice2[indexLinie2][i] = Integer.valueOf(matrice[i]);
                    }
                    indexLinie2++;
                    linie = reader.readLine();
                }
                parametri.setMatrix1(matrice1);
                parametri.setMatrix2(matrice2);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return parametri;
    }

    int[][] adunMatrici(Parametri parametri) {
        int[][] sum = new int[parametri.getNrLinii()][parametri.getNrCol()];
        for (int i = 0; i < parametri.getNrLinii(); i++) {
            for (int j = 0; j < parametri.getNrCol(); j++) {
                sum[i][j] = parametri.getMatrix1()[i][j] + parametri.getMatrix2()[i][j];
            }
        }
        return sum;
    }

    void scriuMatriceaSumaInFisier(int[][] sum) {
        File file = new File("C:\\Users\\vasile\\Desktop\\sumMatrici.txt");

        try {
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write("Suma matricilor din fisierul initial este: ");
            bufferedWriter.newLine();
            for (int i = 0; i < sum.length; i++) {
                for (int j = 0; j < sum.length; j++) {
                    bufferedWriter.write(Integer.valueOf(sum[i][j]) + " ");
                }
                bufferedWriter.newLine();
            }
            bufferedWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
