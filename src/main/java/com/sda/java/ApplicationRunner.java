package com.sda.java;

/**
 * Hello world!
 *
 */
public class ApplicationRunner
{
    public static void main( String[] args )
    {
     AdunMatrici adunMatrici = new AdunMatrici();

     Parametri parametri = adunMatrici.citescMatricile("C:\\Users\\vasile\\Desktop\\start.txt");
     int [][] sum = adunMatrici.adunMatrici(parametri);
     adunMatrici.scriuMatriceaSumaInFisier(sum);
    }
}
